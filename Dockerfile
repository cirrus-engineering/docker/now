FROM node:alpine

RUN npm i -g --unsafe-perm now

CMD [ "node" ]
