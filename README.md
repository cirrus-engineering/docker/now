# Now CLI Docker Image

An image with Now CLI preinstalled, for extra fastness when using Now in CI.

### Authentication

Just create a Runner variable called *NOW_TOKEN*, then run a `now` command with the `--token` flag. E.g.:

```bash
now --token $NOW_TOKEN && now --token $NOW_TOKEN alias
# OR
now -t $NOW_TOKEN
```